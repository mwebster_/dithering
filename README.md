# DITHERT

[![PyPI](https://img.shields.io/pypi/l/fsfe-reuse.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/reuse/reuse)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)


![Screen Shot 2017-11-16 at 11.03.33.png](https://bitbucket.org/repo/5qaRB7n/images/3923687722-Screen%20Shot%202017-11-16%20at%2011.03.33.png)
---

## Introduction

I came across some interesting articles on dither algorithms and consequently became interested in implementing some simple sketches with Processing.
What ensues is an exploration of dither for letter forms and continues ideas developed during the alphaGraph project and workshops.

## Contents

* Algorithms
*
* [...]

## Install

The programs in this repository must be compiled using the Processing environment.

https://processing.org/

## Contact & Sundries

* mark.webster[at]wanadoo.fr
* Version v0.15
* Tools used : Processing

## Contribute
To contribute to this project, please fork the repository and make your contribution to the
fork, then open a pull request to initiate a discussion around the contribution.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/
